import React, { Component } from 'react';
import RNSoundLevel from 'react-native-sound-level';

import { Text, View, Button, StyleSheet, TouchableOpacity } from 'react-native';
import styled from 'styled-components/native';


const Botao = styled.Button`
  
    padding: 20px;
    background: red;



`;

const Page = styled.View`
    flex: 1;
    justify-content: center;
    align-items: center;

`;






const AreaButtons = styled.View`
    margin-top: 30px;
    display: flex;
    flex-direction: column;

`;

const Bola = styled.View`
    width: 300px;
    height: 300px;
    border: 2px solid #57B894;
    display: flex;
    justify-content: center;
    align-items: center;
    border-radius: 200px;
    background: #57B894;


`;


const ws = new WebSocket('ws://192.168.0.104:3030');


export default class App extends Component {
    constructor(props) {
        super(props);
        this.state = {
            sound: {},
        };

    }



    render() {

        const startRecord = () => {
            RNSoundLevel.start();

            RNSoundLevel.onNewFrame = data => {

                this.setState({ sound: data })
                ws.send((6 * Math.log(this.state.sound.rawValue)).toFixed(2));

            }
        }


        const stopRecord = () => {
            RNSoundLevel.stop();
        }

        return (
            <Page>

                <Text style={{ fontFamily: 'Digital-Regular', fontSize: 25, fontWeight: 'bold', marginBottom: 20 }} > Sistema de controle de ruído  </Text>

                <Bola>

                    <Text style={{ fontSize: 40, fontWeight: 'bold' }}>
                        {(6 * Math.log(this.state.sound.rawValue)).toFixed(2)} DB
                    </Text>
                </Bola>

                <AreaButtons>
                    <Botao  title="Iniciar Captura" onPress={startRecord} />
                    <Botao title="Parar Captura" onPress={stopRecord} />

                </AreaButtons>


      


            </Page>



        );
    }
}

const styles = StyleSheet.create({
    botao: {
        marginBottom: 1,



    }




});