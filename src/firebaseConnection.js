import firebase from 'firebase/app';
import 'firebase/database';


let firebaseConfig = {
    apiKey: "AIzaSyDC4Az3TUVJQhzgTx2_tsiOH2ui3l0bDQg",
    authDomain: "teste-3a35b.firebaseapp.com",
    databaseURL: "https://teste-3a35b.firebaseio.com",
    projectId: "teste-3a35b",
    storageBucket: "teste-3a35b.appspot.com",
    messagingSenderId: "443730484782",
    appId: "1:443730484782:web:62a9f12bb3905ad8d9c9bf"
};

if (!firebase.apps.length) {
    firebase.initializeApp(firebaseConfig);

}

export default firebase;

